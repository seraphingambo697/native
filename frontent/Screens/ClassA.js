import React, { Component } from 'react'
import { StyleSheet, Text, View, Button } from 'react-native';

export class ClassA extends Component {

  state = {
      name: "Parwiz"
  }  
  render() {
    return (
        <View>
          
             <Text style = {{fontSize:25}}>
                {this.state.name}
            </Text>
            <Button title = "Click Me" onPress={() => this.setState({name:"Text changed"})}/>

        </View>
    )
  }
}

export default ClassA