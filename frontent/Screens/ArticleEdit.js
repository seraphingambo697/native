import React, {useState} from 'react'
import { StyleSheet, Text, View, FlatList, Alert, ScrollView } from 'react-native';
import {TextInput, Button } from  'react-native-paper';

function ArticleEdit(props) {
    const data = props.route.params.data;

    const [title, setTitle] = useState(data.title)
    const [description, setDescription] = useState(data.description)

    const updateData =() => {
        fetch(`http://127.0.0.1:8000/api/article/${data.id}/`,{
        method:"PUT",
        headers: {
                'Content-type':'application/json'
        },
        body: JSON.stringify({title:title, description:description})
    })
    .then(resp => resp.json())
    .then(data => {
        //console.log(data)
        props.navigation.navigate("Home", {data:data})
    })
    .catch(error => Alert.alert('error', error))
    }
  return (
    <View>
        <TextInput style = {styles.inputStyle}
          label= "Title"
          value={title}
          mode = "outlined"

          onChangeText = {text => setTitle(text)}
        />

        <TextInput style = {styles.inputStyle}
          label= "Description"
          value={description}
          mode = "outlined"
          multiline
          numberOfLines={10}
          onChangeText = {text => setDescription(text)}
        />

        <Button style = {{marginTop:10}}
            icon = "update"
            mode = "contained"
            onPress={() => updateData()}
        >Update Article</Button>
    </View>
  )
}
const styles = StyleSheet.create({
    inputStyle: {
      padding:10, 
      margin:5,
      marginTop:25
    }
  })

export default ArticleEdit