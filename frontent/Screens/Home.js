import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, FlatList, Alert } from 'react-native';
import {Card, FAB} from 'react-native-paper';


/*const mydata = [
    {id:1, title:"First Title", description:"First description"},
    {id:2, title:"Second Title", description:"Second description"},
    {id:3, title:"Third Title", description:"Third description"},
    {id:4, title:"Fourth TiTle", description:"Fourth description"},
    {id:3, title:"Third Title", description:"Third description"},
    {id:4, title:"Fourth TiTle", description:"Fourth description"},
    {id:3, title:"Third Title", description:"Third description"},
    {id:4, title:"Fourth TiTle", description:"Fourth description"},
    {id:3, title:"Third Title", description:"Third description"},
    {id:4, title:"Fourth TiTle", description:"Fourth description"},
    {id:3, title:"Third Title", description:"Third description"},
    {id:4, title:"Fourth TiTle", description:"Fourth description"},
    {id:3, title:"Third Title", description:"Third description"},
    {id:4, title:"Fourth TiTle", description:"Fourth description"},
   
] */



function Home(props) {
    
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)

  const loadData = () =>  {
    fetch('http://127.0.0.1:8000/api/article/',{
        method:"GET"
    })
    .then(resp => resp.json())
    .then(data => {
        setData(data)
        setLoading(false)
        //console.log(data, "ce sont les datas ")
    })
    .catch(error => console.log("Error"))
  }

  useEffect (() => {
    loadData ();
  }, [])

  const clickedItem = (data) => {
      props.navigation.navigate("Details", {data:data})
  }


  const renderData = (item) => {
    return(
        <Card style = {styles.cardStyle} onPress = {() => clickedItem(item)}>
            <Text style = {{fontSize:12}}>{item.title}</Text>
            <Text style = {{fontSize:12}}>{item.description}</Text>
        </Card>
    )
  }
  
  return (
      <View style = {{flex:1}}>
        <FlatList 
            data = {data}
            renderItem = {({item}) => {
                console.log(item)
                return renderData(item)
            }}
            onRefresh = {() => loadData()}
            refreshing = {loading}
            keyExtractor={item => `${item.id}`}
        />

        <FAB
         style = {styles.fab}
         small = {false}
         icon = "plus"
         theme = {{colors:{accent:"blue"}}}  
         //onPress ={() => console.log("Pressed")}
         onPress ={() => props.navigation.navigate("Create")}
        />
        </View>
  )
}

const styles = StyleSheet.create({
    cardStyle: {
        padding:10,
        margin:10
    },
    fab: {
        position:'absolute',
        margin:16,
        right:0,
        bottom:0,
        //backgroundColor:"blue"
    }
})

export default Home