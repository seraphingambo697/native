import React , {useState} from 'react'
import {StyleSheet, Text, View } from 'react-native';
import {TextInput, Button } from  'react-native-paper';


function Create(props) {
  const [title, setTitle] = useState("")
  const [description, setDescription] = useState("")


  const InsertData =() => {
    fetch(`http://127.0.0.1:8000/api/article/`,{
    method:"POST",
    headers: {
            'Content-type':'application/json'
    },
    body: JSON.stringify({title:title, description:description})
    })
    .then(resp => resp.json())
    .then(data => {
        //console.log(data, "insert data")
        props.navigation.navigate("Home")
    })
    .catch(error => console.log('error'))
  }

  return (
    <View>
        <TextInput style = {styles.inputStyle}
          label= "Title"
          value={title}
          mode = "outlined"

          onChangeText = {text => setTitle(text)}
        />

        <TextInput style = {styles.inputStyle}
          label= "Description"
          value={description}
          mode = "outlined"
          multiline
          numberOfLines={10}
          onChangeText = {text => setDescription(text)}
        />

        <Button style = {{marginTop:10}}
            icon = "pencil"
            mode = "contained"
            onPress={() => InsertData()}
        >Insert Article</Button>
    </View>
  )
}

const styles = StyleSheet.create({
  inputStyle: {
    padding:10, 
    margin:5,
    marginTop:25
  }
})
export default Create 