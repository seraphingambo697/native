from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    #path('articles/', article_list, name="articles"),
    #path('article_details/', article_details, name="article_details"),
    path('article/', ArticleList.as_view(), name="article"),
    path('article/<int:id>/', ArticleDetails.as_view(), name="article"),
]